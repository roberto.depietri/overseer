FROM igwn/base:buster
LABEL name="igwn-alert overseer" \
      maintainer="alexander.pace@ligo.org" \
      date="20211129"

# Make a directory for the overseer source code:
RUN mkdir src

# Copy the source code over to the container:
ADD . src/

# Install pip:
RUN apt-get update && \
    apt-get install --install-recommends --assume-yes \
        python3-pip \
        python3.7 && \
    apt-get clean 

# Upgrade pip:
RUN pip3 install --upgrade pip 

# Install overseer:
RUN cd src && \
    pip3 install .

# Copy over the supervisord control file to the correct location:
COPY docker/supervisord-overseer.conf /etc/supervisor/conf.d/overseer.conf

# Make an 'overseer' user to run via supervisor:
RUN groupadd overseer
RUN useradd -M -u 50001 -g overseer -s /bin/false overseer

# Copy over the entrypoint script. This pulls in secrets
# from docker secrets.
COPY docker/entrypoint /usr/local/bin/entrypoint
RUN chmod 0755 /usr/local/bin/entrypoint

# Install supervisor:
RUN pip3 install supervisor

# Turn on the overseer. This is probably overkill, but whatever. 
ENV ENABLE_OVERSEER true

# Expose the overseer port: 
EXPOSE 8002

# Close out
ENTRYPOINT [ "/usr/local/bin/entrypoint" ]
CMD ["/usr/local/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]

